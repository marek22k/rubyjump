require "net/http"
require "gdbm"

require_relative "config.rb"

puts "Open/create database"
$db = GDBM.new $gdbm_file

uri = URI(ARGV[0])

puts "Connect"
Net::HTTP.new(uri.host, nil, ARGV[1], ARGV[2].to_i).start { |http|
  puts "Get"
  resp = http.get(uri)
  if resp.code == "200"
    puts "Save to #{$gdbm_file}"
    hosts = resp.body.lines
    hosts.each { |str|
      ind = str.index("=")
      $db[str[0...ind]] = str[ind+1..-1].chomp
    }
  else
    puts "Error: #{resp.code} - #{resp.inspect}"
  end
}